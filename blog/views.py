from django.shortcuts import render

# Dummy data
posts = [
    {
        'author': 'Steven Mann',
        'title': 'Blog 1',
        'content': 'content 1',
        'date_posted': 'September 16, 2019'
    },
    {
        'author': 'Jane Doe',
        'title': 'Blog 2',
        'content': 'content 2',
        'date_posted': 'September 17, 2019'
    }
]

# Create your views here.
def home(request):
    context = {
        'posts': posts,
        'title': 'Home'
    }
    return render(request, 'blog/home.html', context)

def about(request):
    return render(request, 'blog/about.html', {'title': 'About'})